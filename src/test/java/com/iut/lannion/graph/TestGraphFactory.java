package com.iut.lannion.graph;

import org.locationtech.jts.geom.Coordinate;

import com.iut.lannion.graph.model.Graph;
import com.iut.lannion.graph.model.Vertex;

public class TestGraphFactory {

	/**
	 * d / / a--b--c
	 * 
	 * @return
	 */
	public static Graph createGraph01() {
		Graph graph = new Graph();

		Vertex a = graph.createVertex(new Coordinate(0.0, 0.0), "a");

		Vertex b = graph.createVertex(new Coordinate(1.0, 0.0), "b");

		Vertex c = graph.createVertex(new Coordinate(2.0, 0.0), "c");

		Vertex d = graph.createVertex(new Coordinate(1.0, 1.0), "d");

		Vertex e = graph.createVertex(new Coordinate(2.0, 2.0), "e");

		graph.createEdge(a, b, "ab");

		graph.createEdge(b, c, "bc");

		graph.createEdge(a, d, "ad");

		graph.createEdge(c, d, "cd");

		graph.createEdge(d, e, "de");

		return graph;
	}

}
