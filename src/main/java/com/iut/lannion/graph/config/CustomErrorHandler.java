package com.iut.lannion.graph.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.iut.lannion.graph.errors.JsonError;
import com.iut.lannion.graph.errors.NotFoundException;

/**
 * [spring] Rendu des exceptions au format JSON
 *
 * @author etassel
 */
@ControllerAdvice
public class CustomErrorHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<JsonError> handleExceptions(Exception e, WebRequest request) {
		JsonError error = new JsonError(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(NotFoundException.class)
	public final ResponseEntity<JsonError> handleNotFoundExceptions(Exception e, WebRequest request) {
		JsonError error = new JsonError(HttpStatus.NOT_FOUND, e.getMessage());
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

}

