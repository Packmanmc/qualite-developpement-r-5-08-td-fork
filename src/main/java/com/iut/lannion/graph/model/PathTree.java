package com.iut.lannion.graph.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.iut.lannion.graph.errors.NotFoundException;
import com.iut.lannion.graph.routing.DijkstraPathFinder;

public class PathTree {

	private static final Logger log = LogManager.getLogger(DijkstraPathFinder.class);

	private Map<Vertex, PathNode> nodes;

	public PathTree(Vertex origin) {
		this.nodes = new HashMap<>();

		/* création d'un nouveau sommet origin */
		this.nodes.put(origin, new PathNode(0.0, null, false, origin));

		log.trace("initGraph({})", origin);
	}

	/**
	 * Construit le chemin en remontant les relations incoming edge
	 * 
	 * @param destination
	 * @return
	 */
	public Path getPath(Vertex destination) {
		Path result = new Path();

		Edge current = this.getOrCreateNode(destination).getReachingEdge();
		if (this.isReached(destination)) {
			do {
				result.getEdges().add(current);
				current = this.getOrCreateNode(current.getSource()).getReachingEdge();
			} while (current != null);
		}

		Collections.reverse(result.getEdges());
		return result;
	}

	private PathNode getNode(Vertex vertex) {
		if (this.nodes.containsKey(vertex)) {
			return this.nodes.get(vertex);
		}
		throw new NotFoundException("Node not found");
	}

	public boolean isReached(Vertex destination) {
		return this.getOrCreateNode(destination).getReachingEdge() != null;
	}

	public PathNode getOrCreateNode(Vertex vertex) {
		PathNode pathNode;
		try {
			pathNode = this.getNode(vertex);
		} catch (NotFoundException e) {
			/* création d'un nouveau sommet car non trouvé */
			pathNode = new PathNode(Double.POSITIVE_INFINITY, null, false, vertex);
			this.nodes.put(vertex, pathNode);
		}
		return pathNode;
	}

	public List<Vertex> getReachedVertices() {
		List<Vertex> result = new ArrayList<>();

		for (Vertex vertex : this.getNodes().keySet()) {
			result.add(vertex);
		}

		return result;
	}

	public Map<Vertex, PathNode> getNodes() {
		return nodes;
	}

	public void setNodes(Map<Vertex, PathNode> nodes) {
		this.nodes = nodes;
	}
}
