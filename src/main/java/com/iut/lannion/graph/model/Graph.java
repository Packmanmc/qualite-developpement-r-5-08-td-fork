package com.iut.lannion.graph.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.locationtech.jts.geom.Coordinate;

import com.iut.lannion.graph.errors.NotFoundException;

/**
 * 
 * Un graphe matérialisé par une liste de sommets et d'arcs
 * 
 * @author etassel
 *
 */
public class Graph {
	/**
	 * Liste des sommets
	 */
	private Map<Coordinate, Vertex> vertices = new HashMap<>();

	/**
	 * Liste des arcs
	 */
	private List<Edge> edges = new ArrayList<>();

	/**
	 * Récupération de la liste sommets
	 * 
	 * @return
	 */
	public Collection<Vertex> getVertices() {
		return vertices.values();
	}

	/**
	 * Recherche d'un sommet par identifiant
	 * 
	 * @param id
	 * @return
	 */
	public Vertex findVertex(String id) {
		for (Vertex vertex : this.getVertices()) {
			if (vertex.getId().equals(id)) {
				return vertex;
			}
		}
		throw new NotFoundException(String.format("Vertex '%s' not found", id));
	}

	/**
	 * Recherche d'un sommet par égalité stricte de coordonnées
	 * 
	 * @param coordinate
	 * @return
	 */
	public Vertex findVertex(Coordinate coordinate) {
		if(this.vertices.containsKey(coordinate)) {
			return this.vertices.get(coordinate);
		}
		
		throw new NotFoundException(String.format("Vertex not found at [%s,%s]", coordinate.x, coordinate.y));
	}

	public Vertex createVertex(Coordinate coordinate, String id) {
		Vertex vertex = new Vertex(); 
		vertex.setId(id); 
		vertex.setCoordinate(coordinate); 
		this.vertices.put(coordinate, vertex);
		
		return vertex;
	}
	
	public Edge createEdge(Vertex source, Vertex target, String id) {
		Edge edge = new Edge(source, target); 
		edge.setId(id);
		this.edges.add(edge);
		
		return edge;
	}
	/**
	 * Récupération ou création d'un sommet en assurant l'unicité
	 * 
	 * @param graph
	 * @param coordinate
	 * @return
	 */
	public Vertex getOrCreateVertex(Coordinate coordinate) {
		Vertex vertex;
		try {
			vertex = findVertex(coordinate);
		} catch (NotFoundException e) {
			/* création d'un nouveau sommet car non trouvé */
			vertex = this.createVertex(coordinate, Integer.toString(getVertices().size()));
		}
		return vertex;
	}

	/**
	 * Récupération de la liste des arcs
	 * 
	 * @return
	 */
	public Collection<Edge> getEdges() {
		return edges;
	}

	/**
	 * Recherche des arcs entrant d'un sommet
	 * 
	 * @param vertex
	 * @return
	 */
	public List<Edge> getInEdges(Vertex vertex) {
		return vertex.getInEdges();
	}

	/**
	 * Recherche des arcs sortant d'un sommet
	 * 
	 * @param vertex
	 * @return
	 */
	public List<Edge> getOutEdges(Vertex vertex) {
		return vertex.getOutEdges();
	}

}
